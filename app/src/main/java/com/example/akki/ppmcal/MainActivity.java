package com.example.akki.ppmcal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private EditText etNeedSoltion;
    private EditText etNeedPPM;
    private EditText etPurity;
    private TextView tvNeedMatirial;
    private Button btCalculate;
    private TextView tvPercentage;
    private Button btExit;
    private Button btnEdir3;
    private  Button btReset;
    double num1,num2,num3,sum,sum2;
    AlertDialog.Builder ab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();
//        btExit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alert();
//            }
//        });
        btCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findPPM();
            }
        });
        btReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });

    }

    private void findPPM() {
        if( etNeedSoltion.getText().toString().equals("")) {
            etNeedSoltion.setError("Enter Solution");

        }
        else if( etNeedPPM.getText().toString().equals("")) {
            etNeedPPM.setError("Enter PPM");

        }
        else if( etPurity.getText().toString().equals("")) {
            etPurity.setError("Enter Percent Purity");

        }
        else {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
            num1 = Double.parseDouble(etNeedSoltion.getText().toString());
            num2 = Double.parseDouble(etNeedPPM.getText().toString());
            num3 = Double.parseDouble(etPurity.getText().toString());
            if (num3 > 100) {
                etPurity.setError("Enter Purity 1 to 100");
                Toast.makeText(MainActivity.this, "Enter Purity 1 to 100", Toast.LENGTH_SHORT).show();
                return;
            }
            else {
                sum = (num2 / 10000);
                sum2 = ((num2 / 1000000 * num1 * 1000) / num3) * 100;
                tvNeedMatirial.setText(new DecimalFormat("##.##").format(sum2));
                etPurity.clearFocus();
                etNeedSoltion.clearFocus();
                etNeedPPM.clearFocus();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_ppm,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_help) {
            Intent help = new Intent(MainActivity.this,Avtivity_Help.class);
            startActivity(help);
        }
        if(id == R.id.action_About){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void alert(){
        ab.setMessage("Do you want to exit").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alr = ab.create();
        alr.setTitle("Exit");
        alr.show();

    }
    public void init(){
        etNeedSoltion = (EditText)findViewById(R.id.editText);
        etNeedPPM = (EditText)findViewById(R.id.editText2);
        etPurity = (EditText)findViewById(R.id.editText3);
        tvNeedMatirial = (TextView)findViewById(R.id.textView6);
        btCalculate = (Button)findViewById(R.id.button3);
//        tvPercentage = (TextView)findViewById(R.id.textView7);
//        btExit = (Button)findViewById(R.id.button2);
        btReset = (Button)findViewById(R.id.button);
        ab = new AlertDialog.Builder(MainActivity.this);
    }
    public void reset(){
        etNeedSoltion.setText("");
        etNeedPPM.setText("");
        etPurity.setText("");
        tvNeedMatirial.setText("");
//        tvPercentage.setText("");
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Exit");
        builder.setMessage("Do you want to exit? ");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
                MainActivity.super.onBackPressed();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
