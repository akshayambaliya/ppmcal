package com.example.akki.ppmcal;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class Avtivity_Help extends AppCompatActivity {
    private TextView tvHelpInfo;
    private TextView tvFinalInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avtivity__help);
        tvHelpInfo = (TextView)findViewById(R.id.textView);
        tvFinalInfo = (TextView)findViewById(R.id.textView2);
        tvFinalInfo.setText(Html.fromHtml("<p>*This calculator only for reference <b>purposes</b> Please check twice and compere with other calculations methods before use</p>"));
       tvHelpInfo.setText(Html.fromHtml("<p><b>To count the material needed in solution.</b></p>\n" +
               "        <p><b>Following example will help you, How to access this application.</b></p>\n" +
               "        <p><b>You want to spray GA3(Gibberellic acid) 50PPM in your lemon crop, you have Gibberellic acid 90% purity and you have 200 liters Drum for making spray solution, Now calculate how many grams Gibberellic acid 90% added in 200 liters water drum for making 50PPM solution.</p></b>\n" +
               "        <ul>\n" +
               ""+
               "            <li>Enter your needed solution ex. For your spray tank capacity in litter here :- 200</li>\n" +
               "            <li>Enter needed PPM :- 50</li>\n" +
               "            <li>Enter product puruty in percentage :- 90</li>\n" +
               "        </ul>\n" +
               "            <p>And Push calculate button.</p>\n" +
               "            <p>You have a result needed Matriels in gram :- 11.11</p>\n" +
               "        <p>You need <b>11.11</b> gram Gibberellic acid <b>90%</b> in <b>200</b> liters water for making 50PPM solution.</p>"
       ));
    }
}
